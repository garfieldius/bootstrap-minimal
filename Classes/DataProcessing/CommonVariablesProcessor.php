<?php
declare(strict_types=1);
namespace GrossbergerGeorg\BootstrapMinimal\DataProcessing;

/*
 * Copyright 2020 by Georg Großberger <contact@grossberger-ge.org>
 *
 * This is free software; it is provided under the terms of Apache License 2.0
 * See the file LICENSE or <https://www.apache.org/licenses/LICENSE-2.0> for details
 */

use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;
use TYPO3\CMS\Frontend\ContentObject\DataProcessorInterface;

/**
 * Add cObject data and the page record as variables
 *
 * @author Georg Großberger <contact@grossberger-ge.org>
 */
class CommonVariablesProcessor implements DataProcessorInterface
{
    public function process(
        ContentObjectRenderer $cObj,
        array $contentObjectConfiguration,
        array $processorConfiguration,
        array $processedData
    ) {
        $processedData['data'] = $cObj->data;
        $processedData['page'] = $GLOBALS['TSFE']->page;

        return $processedData;
    }
}
