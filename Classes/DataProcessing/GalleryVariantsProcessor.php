<?php
declare(strict_types=1);
namespace GrossbergerGeorg\BootstrapMinimal\DataProcessing;

/*
 * Copyright 2020 by Georg Großberger <contact@grossberger-ge.org>
 *
 * This is free software; it is provided under the terms of Apache License 2.0
 * See the file LICENSE or <https://www.apache.org/licenses/LICENSE-2.0> for details
 */

use GrossbergerGeorg\BootstrapMinimal\CropVariants\CropVariantsRenderer;
use TYPO3\CMS\Core\Resource\File;
use TYPO3\CMS\Core\Resource\FileRepository;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;
use TYPO3\CMS\Frontend\ContentObject\DataProcessorInterface;

/**
 * Gallery
 *
 * @author Georg Großberger <contact@grossberger-ge.org>
 */
class GalleryVariantsProcessor implements DataProcessorInterface
{
    public function process(
        ContentObjectRenderer $cObj,
        array $contentObjectConfiguration,
        array $processorConfiguration,
        array $processedData
    ) {
        $table = $processorConfiguration['table'] ?? 'tt_content';
        $field = $processorConfiguration['field'] ?? 'image';
        $variantSet = $processorConfiguration['variantSet'] ?? 'default';

        $references = GeneralUtility::makeInstance(FileRepository::class)->findByRelation(
            $table,
            $field,
            $cObj->data['uid']
        );

        $renderer = GeneralUtility::makeInstance(CropVariantsRenderer::class, $table, $field, $variantSet);
        $options = $processorConfiguration['rendererOptions'] ?? null;

        if (is_array($options)) {
            $renderer->mergeOptions($options);
        }

        $gallery = $renderer->generateVariants($references);
        $as = $processorConfiguration['as'] ?? 'gallery';

        $processedData[$as] = $gallery;

        return $processedData;
    }
}
