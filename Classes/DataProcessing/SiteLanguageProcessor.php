<?php
declare(strict_types=1);
namespace GrossbergerGeorg\BootstrapMinimal\DataProcessing;

/*
 * Copyright 2020 by Georg Großberger <contact@grossberger-ge.org>
 *
 * This is free software; it is provided under the terms of Apache License 2.0
 * See the file LICENSE or <https://www.apache.org/licenses/LICENSE-2.0> for details
 */

use Psr\Http\Message\RequestInterface;
use TYPO3\CMS\Core\Site\Entity\SiteLanguage;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;
use TYPO3\CMS\Frontend\ContentObject\DataProcessorInterface;

/**
 * Add the current language of the site as variable
 *
 * @author Georg Großberger <contact@grossberger-ge.org>
 */
class SiteLanguageProcessor implements DataProcessorInterface
{
    public function process(
        ContentObjectRenderer $cObj,
        array $contentObjectConfiguration,
        array $processorConfiguration,
        array $processedData
    ) {
        $request = $GLOBALS['TYPO3_REQUEST'] ?? null;

        if ($request instanceof RequestInterface && $request->getAttribute('language')) {
            $language = $request->getAttribute('language');

            if ($language instanceof SiteLanguage) {
                $as = $processorConfiguration['as'] ?? 'language';
                $processedData[$as] = $language;
            }
        }

        return $processedData;
    }
}
