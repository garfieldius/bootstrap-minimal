<?php
declare(strict_types=1);
namespace GrossbergerGeorg\BootstrapMinimal\CropVariants;

/*
 * Copyright 2020 by Georg Großberger <contact@grossberger-ge.org>
 *
 * This is free software; it is provided under the terms of Apache License 2.0
 * See the file LICENSE or <https://www.apache.org/licenses/LICENSE-2.0> for details
 */

use TYPO3\CMS\Backend\Form\Element\ImageManipulationElement;
use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * @author Georg Großberger <contact@grossberger-ge.org>
 */
class ExtendedVariantsAwareManipulationElement extends ImageManipulationElement
{
    protected function populateConfiguration(array $baseConfiguration)
    {
        $config = parent::populateConfiguration($baseConfiguration);
        $config['cropVariants'] = $this->extendCropVariants();

        return $config;
    }

    private function extendCropVariants(): array
    {
        $loader = GeneralUtility::makeInstance(CropVariantsLoader::class);
        $parent = [];

        if (!empty($this->data['inlineParentUid'])) {
            $parent = BackendUtility::getRecord($this->data['inlineParentTableName'], $this->data['inlineParentUid']) ?? [];
        }

        $variants = [];
        $configured = $loader->getForRecord(
            $this->data['inlineParentTableName'] ?? $this->data['tableName'] ?? 'default',
            $this->data['inlineParentFieldName'] ?? 'default',
            $parent,
            $this->data['databaseRow'] ?? []
        );

        foreach ($configured as $i => $crop) {
            $variants[$i] = [
                'title'               => $crop['title'],
                'selectedRatio'       => key($crop['aspects']),
                'cropArea'            => $crop['cropArea'],
                'allowedAspectRatios' => $crop['aspects'],
            ];
        }

        return $variants;
    }
}
