<?php
declare(strict_types=1);
namespace GrossbergerGeorg\BootstrapMinimal\CropVariants;

/*
 * Copyright 2020 by Georg Großberger <contact@grossberger-ge.org>
 *
 * This is free software; it is provided under the terms of Apache License 2.0
 * See the file LICENSE or <https://www.apache.org/licenses/LICENSE-2.0> for details
 */

use TYPO3\CMS\Core\Cache\CacheManager;
use TYPO3\CMS\Core\Package\PackageManager;
use TYPO3\CMS\Core\SingletonInterface;
use TYPO3\CMS\Core\Utility\ArrayUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * CropVariantsResolver
 *
 * @author Georg Großberger <contact@grossberger-ge.org>
 */
class CropVariantsLoader implements SingletonInterface
{
    private $configuration = [];

    public function __construct()
    {
        $cache = GeneralUtility::makeInstance(CacheManager::class)->getCache('bootstrap_minimal');
        $config = $cache->get('crop');

        if (is_array($config)) {
            $this->configuration = $config;
        } else {
            $packageManager = GeneralUtility::makeInstance(PackageManager::class);
            $config = [];

            foreach ($packageManager->getActivePackages() as $package) {
                $file = $package->getPackagePath() . 'Configuration/CropVariants.yml';

                if (is_file($file)) {
                    $data = @yaml_parse_file($file);

                    if (is_array($data)) {
                        $config = array_replace_recursive($config, $data);
                    }
                }
            }

            $default = $config['default'];
            unset($config['default']);
            $this->configuration['default'] = $default;

            foreach ($config as $key => $settings) {
                $overrides = $default;
                ArrayUtility::mergeRecursiveWithOverrule($overrides, $settings);
                $this->configuration[$key] = $overrides;
            }

            $cache->set('crop', $this->configuration);
        }
    }

    public function getForRecord(string $table, string $field, array $parent, array $self): array
    {
        $config = $this->configuration['default'];

        foreach ($this->configuration as $k => $overrides) {
            if (($overrides['table'] ?? null) === $table && ($overrides['field'] ?? null) === $field) {
                $requiredMatches = 0;
                $actualMatches = 0;

                foreach (['self' => $self, 'parent' => $parent] as $section => $record) {
                    foreach ($overrides['_if'][$section] ?? [] as $column => $expectedValue) {
                        $requiredMatches++;

                        if (isset($record[$column])) {
                            $actualValue = $record[$column];

                            if (!is_array($actualValue)) {
                                $actualValue = [$actualValue];
                            }

                            if (!in_array($expectedValue, $actualValue)) {
                                $actualMatches++;
                            }
                        }
                    }
                }

                if ($requiredMatches === $actualMatches) {
                    $config = $overrides;
                }
            }
        }

        unset($config['table'], $config['field'], $config['_if']);

        return $config;
    }

    public function getVariantSet(string $name): array
    {
        $set = $this->configuration[$name] ?? null;

        if (!is_array($set) || empty($set)) {
            throw new \InvalidArgumentException('No variant set with name ' . $name);
        }

        return $set;
    }
}
