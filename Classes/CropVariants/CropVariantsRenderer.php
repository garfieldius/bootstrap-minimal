<?php
declare(strict_types=1);
namespace GrossbergerGeorg\BootstrapMinimal\CropVariants;

/*
 * Copyright 2020 by Georg Großberger <contact@grossberger-ge.org>
 *
 * This is free software; it is provided under the terms of Apache License 2.0
 * See the file LICENSE or <https://www.apache.org/licenses/LICENSE-2.0> for details
 */

use TYPO3\CMS\Core\Imaging\ImageManipulation\CropVariantCollection;
use TYPO3\CMS\Core\Resource\FileReference;
use TYPO3\CMS\Core\Resource\Rendering\RendererRegistry;
use TYPO3\CMS\Core\Utility\ArrayUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Service\ImageService;

/**
 * CropVariantsRenderer
 *
 * @author Georg Großberger <contact@grossberger-ge.org>
 */
class CropVariantsRenderer
{
    /**
     * @var string
     */
    private string $table;

    /**
     * @var string
     */
    private string $field;

    /**
     * @var string
     */
    private string $variantSet;

    private array $rendererOptions = [
        'defaultWidth'         => 0,
        'defaultHeight'        => 0,
        'autoplay'             => null,
        'controls'             => null,
        'muted'                => null,
        'loop'                 => null,
        'allow'                => null,
        'modestbranding'       => true,
        'relatedVideos'        => false,
        'no-cookie'            => true,
        'showinfo'             => false,
        'additionalAttributes' => ['class' => 'embed-responsive-item'],
    ];

    /**
     * @var RendererRegistry
     */
    private $rendererRegistry;
    /**
     * @var ImageService
     */
    private $imageService;

    /**
     * CropVariantsRenderer constructor.
     * @param string $table
     * @param string $field
     * @param string $variantSet
     * @param $rendererRegistry
     * @param $imageService
     */
    public function __construct(
        string $table,
        string $field,
        string $variantSet,
        RendererRegistry $rendererRegistry = null,
        ImageService $imageService = null
    ) {
        $this->table = $table;
        $this->field = $field;
        $this->variantSet = $variantSet;
        $this->rendererRegistry = $rendererRegistry ?? RendererRegistry::getInstance();
        $this->imageService = $imageService ?? GeneralUtility::makeInstance(ImageService::class);
    }

    public function generateVariants(array $references): array
    {
        $variants = GeneralUtility::makeInstance(CropVariantsLoader::class)->getVariantSet($this->variantSet);
        $results = [];

        /** @var FileReference $reference */
        foreach ($references as $reference) {
            $result = [
                'original' => $reference,
            ];
            $renderer = $this->rendererRegistry->getRenderer($reference);

            if ($renderer) {
                $result['html'] = $renderer->render(
                    $reference,
                    $this->rendererOptions['mediaWidth'],
                    $this->rendererOptions['mediaHeight'],
                    $this->rendererOptions
                );
            } else {
                $cropVariantCollection = CropVariantCollection::create((string) $reference->getProperty('crop'));

                foreach ($variants as $name => $config) {
                    $width = (int) $config['width'];
                    $height = (int) $config['height'];
                    $cropArea = $cropVariantCollection->getCropArea($name);
                    $crop = $cropArea->isEmpty() ? null : $cropArea->makeAbsoluteBasedOnFile($reference);
                    $processingInstructions = [
                        'width'  => $width . 'm',
                        'height' => $height . 'm',
                        'crop'   => $crop,
                    ];

                    $processedImage = $this->imageService->applyProcessingInstructions($reference, $processingInstructions);

                    $result[$name] = [
                        'urls' => [
                            '1x' => $this->imageService->getImageUri($processedImage),
                        ],
                        'width'       => $processedImage->getProperty('width'),
                        'height'      => $processedImage->getProperty('height'),
                        'mediaQuery'  => $config['mediaQuery'],
                        'columnClass' => round(12 / ($config['columns'] ?? 1)),
                    ];

                    foreach (GeneralUtility::intExplode(',', $config['scaleFactors']) as $scaling) {
                        if ($scaling < 1 || $scaling > 6) {
                            continue;
                        }

                        $processingInstructions = [
                            'width'  => ($width * $scaling) . 'm',
                            'height' => ($height * $scaling) . 'm',
                            'crop'   => $crop,
                        ];
                        $processedImage = $this->imageService->applyProcessingInstructions($reference, $processingInstructions);
                        $result[$name]['urls'][$scaling . 'x'] = $this->imageService->getImageUri($processedImage);
                    }
                }
            }

            $results[] = $result;
        }

        return $results;
    }

    public function mergeOptions(array $options): void
    {
        ArrayUtility::mergeRecursiveWithOverrule($this->rendererOptions, $options);
    }
}
