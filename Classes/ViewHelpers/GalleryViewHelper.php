<?php
declare(strict_types=1);
namespace GrossbergerGeorg\BootstrapMinimal\ViewHelpers;

/*
 * Copyright 2020 by Georg Großberger <contact@grossberger-ge.org>
 *
 * This is free software; it is provided under the terms of Apache License 2.0
 * See the file LICENSE or <https://www.apache.org/licenses/LICENSE-2.0> for details
 */

use GrossbergerGeorg\BootstrapMinimal\CropVariants\CropVariantsRenderer;
use TYPO3\CMS\Core\Resource\FileRepository;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;
use TYPO3Fluid\Fluid\Core\ViewHelper\Traits\CompileWithRenderStatic;

/**
 * Description
 *
 * @author Georg Großberger <g.grossberger@supseven.at>
 */
class GalleryViewHelper extends AbstractViewHelper
{
    use CompileWithRenderStatic;

    protected $escapeOutput = false;

    protected $escapeChildren = false;

    public function initializeArguments()
    {
        $this->registerArgument('table', 'string', '', true);
        $this->registerArgument('field', 'string', '', true);
        $this->registerArgument('variantSet', 'string', '', true);
        $this->registerArgument('rendererOptions', 'array', '');
        $this->registerArgument('uid', 'int', '');
        $this->registerArgument('references', 'array', '');
        $this->registerArgument('as', 'string', '', false, 'gallery');
    }

    public static function renderStatic(
        array $arguments,
        \Closure $renderChildrenClosure,
        RenderingContextInterface $renderingContext
    ) {
        $table = $arguments['table'];
        $field = $arguments['field'];
        $uid = $arguments['uid'];
        $variantSet = $arguments['variantSet'];

        $references = $arguments['references'];
        $references ??= GeneralUtility::makeInstance(FileRepository::class)->findByRelation(
            $table,
            $field,
            $uid
        );

        $renderer = GeneralUtility::makeInstance(CropVariantsRenderer::class, $table, $field, $variantSet);
        $options = $arguments['rendererOptions'] ?? null;

        if (is_array($options)) {
            $renderer->mergeOptions($options);
        }

        $gallery = $renderer->generateVariants($references);
        $as = $arguments['as'];

        $variables = $renderingContext->getVariableProvider();
        $variables->add($as, $gallery);
        $content = (string) $renderChildrenClosure();
        $variables->remove($as);

        return $content;
    }
}
