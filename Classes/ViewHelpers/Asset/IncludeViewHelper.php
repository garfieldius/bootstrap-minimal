<?php
declare(strict_types=1);
namespace GrossbergerGeorg\BootstrapMinimal\ViewHelpers\Asset;

/*
 * Copyright 2020 by Georg Großberger <contact@grossberger-ge.org>
 *
 * This is free software; it is provided under the terms of Apache License 2.0
 * See the file LICENSE or <https://www.apache.org/licenses/LICENSE-2.0> for details
 */

/**
 * Include the given assets content
 *
 * Adds a script/style tag if it is a js/css file
 *
 * @author Georg Großberger <contact@grossberger-ge.org>
 */
class IncludeViewHelper extends AbstractAssetHelper
{
    public function initializeArguments()
    {
        parent::initializeArguments();
        $this->registerArgument('attr', 'array', 'Attributes to add');
    }

    protected static function processFile(string $file, bool $globbed, array $arguments): string
    {
        $content = file_get_contents($file);
        $suffix = strtolower(pathinfo($file, PATHINFO_EXTENSION));

        if ($suffix === 'css' || $suffix === 'js') {
            $tag = $suffix === 'js' ? 'script' : 'style';
            $attributes = $suffix === 'js' ? ' type="text/javascript"' : ' type="text/css"';

            foreach ($arguments['attr'] ?? [] as $name => $value) {
                $value = (string) $value;
                $attributes .= ' ' . htmlspecialchars($name, ENT_COMPAT | ENT_HTML5, 'UTF-8', false);
                $normalizedValue = strtolower(trim($value));

                if (!in_array($normalizedValue, ['1', 'true'], true)) {
                    $attributes .= '="' . htmlspecialchars($value, ENT_COMPAT | ENT_HTML5, 'UTF-8', false) . '"';
                }
            }

            $content = '<' . $tag . $attributes . '>' . $content . '</' . $tag . '>';
        }

        return $content;
    }
}
