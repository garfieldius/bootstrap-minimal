<?php
declare(strict_types=1);
namespace GrossbergerGeorg\BootstrapMinimal\ViewHelpers\Asset;

/*
 * Copyright 2020 by Georg Großberger <contact@grossberger-ge.org>
 *
 * This is free software; it is provided under the terms of Apache License 2.0
 * See the file LICENSE or <https://www.apache.org/licenses/LICENSE-2.0> for details
 */

use Closure;
use TYPO3\CMS\Core\Core\Environment;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;
use TYPO3Fluid\Fluid\Core\ViewHelper\Traits\CompileWithContentArgumentAndRenderStatic;

/**
 * Embed SVG directly into HTML. All SVG data is collected
 * and added just once, right after the opening body tag.
 * This allows for a SVG to be used many times, but the
 * displayed usages only use a reference.
 *
 * @author Georg Großberger <contact@grossberger-ge.org>
 */
class SvgViewHelper extends AbstractViewHelper
{
    use CompileWithContentArgumentAndRenderStatic;

    const OPEN_TAG = '<svg style="display:none;" aria-hidden="true">';

    protected $escapeOutput = false;

    protected $escapeChildren = false;

    /**
     * @var array|[]string
     */
    private static $loaded = [];

    public function initializeArguments()
    {
        $basePath = Environment::getProjectPath() . '/node_modules/@fortawesome/fontawesome-free/svgs/';
        $this->registerArgument('id', 'string', 'Name of icon file without .svg suffix, relativ to path');
        $this->registerArgument('class', 'string', 'CSS class to add');
        $this->registerArgument('role', 'string', 'ARIA role attribute');
        $this->registerArgument('path', 'string', 'Path to icons, absolute or relative to script. Defaults to [PROJECT_ROOT]/node_modules/@fortawesome/fontawesome-free/svgs/', false, $basePath);
        $this->registerArgument('attributes', 'array', 'Additional HTML attributes', false, []);
        $this->registerArgument('data', 'array', 'data-attributes to add', false, []);
    }

    public static function renderStatic(
        array $arguments,
        Closure $renderChildrenClosure,
        RenderingContextInterface $renderingContext
    ) {
        $id = trim((string) $renderChildrenClosure());

        // Ensure "name" to be a valid HTML id attribute and URL safe value
        $name = trim(preg_replace('/[^a-z0-9]+/i', '-', $id), '-');

        // Load only once
        if (!isset(self::$loaded[$name])) {
            $file = GeneralUtility::getFileAbsFileName($arguments['path']) . $id . '.svg';

            if (!is_file($file)) {
                return '<!-- ERROR: SVG ' . $file . ' not found -->';
            }

            $xml = new \SimpleXMLElement(file_get_contents($file));
            $viewBox = (string) $xml->attributes()->viewBox;
            $symbol = '<symbol id="' . $name . '" viewBox="' . $viewBox . '">';

            /** @var \SimpleXMLElement $element */
            foreach ($xml->children() as $element) {
                $tag = (string) $element->getName();

                if ($tag && $tag !== 'desc' && $tag !== 'metadata') {
                    $symbol .= (string) $element->asXML();
                }
            }

            $symbol .= '</symbol>';
            self::$loaded[$name] = $symbol;
        }

        $content = '<svg';

        if ($arguments['class']) {
            $arguments['attributes']['class'] = $arguments['class'];
        }

        if ($arguments['role']) {
            $arguments['attributes']['role'] = $arguments['role'];
        }

        foreach ($arguments['attributes'] as $key => $attr) {
            $content .= ' ' . htmlspecialchars($key);

            if (!empty($attr) && strtolower($attr) !== 'true') {
                $content .= '="' . htmlspecialchars($attr) . '"';
            }
        }

        foreach ($arguments['data'] as $key => $attr) {
            if (!empty($key)) {
                $key = 'data-' . str_replace(' ', '-', strtolower(preg_replace('/([a-z])([A-Z])/', '$1 $2', $key)));
                $content .= ' ' . htmlspecialchars($key);

                if (!empty($attr) && strtolower($attr) !== 'true') {
                    $content .= '="' . htmlspecialchars($attr) . '"';
                }
            }
        }

        return $content . '><use href="#' . $name . '" xlink:href="#' . $name . '"></use></svg>';
    }

    public function prependIconData()
    {
        if (!empty(self::$loaded)) {
            $content = '';
            $tagPos = strpos($GLOBALS['TSFE']->content, self::OPEN_TAG);

            if ($tagPos !== false) {
                $content = substr(
                    $GLOBALS['TSFE']->content,
                    $tagPos + strlen(self::OPEN_TAG),
                    strpos($GLOBALS['TSFE']->content, '</svg>', $tagPos) - $tagPos - strlen(self::OPEN_TAG)
                );

                $left = substr($GLOBALS['TSFE']->content, 0, $tagPos);
                $right = substr($GLOBALS['TSFE']->content, strpos($GLOBALS['TSFE']->content, '</svg>', $tagPos) + 6);
            } else {
                $bodyPos = strpos($GLOBALS['TSFE']->content, '<body');
                $left = substr($GLOBALS['TSFE']->content, 0, $bodyPos);
                $right = substr($GLOBALS['TSFE']->content, strpos($GLOBALS['TSFE']->content, '>', $bodyPos + 5) + 1);
            }

            foreach (self::$loaded as $id => $symbol) {
                if (strpos($content, 'id="' . $id . '"') === false) {
                    $content .= $symbol;
                }
            }

            $GLOBALS['TSFE']->content =
                $left .
                self::OPEN_TAG .
                preg_replace('/>\\s+</', '><', $content) .
                '</svg>' .
                $right;

            self::$loaded = [];
        }
    }
}
