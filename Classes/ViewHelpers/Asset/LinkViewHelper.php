<?php
declare(strict_types=1);
namespace GrossbergerGeorg\BootstrapMinimal\ViewHelpers\Asset;

/*
 * Copyright 2020 by Georg Großberger <contact@grossberger-ge.org>
 *
 * This is free software; it is provided under the terms of Apache License 2.0
 * See the file LICENSE or <https://www.apache.org/licenses/LICENSE-2.0> for details
 */

use TYPO3\CMS\Core\Utility\PathUtility;

/**
 * Generate a link to the given assets.
 *
 * If the filename has no cache bust, the
 * versionInFilename setting is applied
 *
 * @author Georg Großberger <contact@grossberger-ge.org>
 */
class LinkViewHelper extends AbstractAssetHelper
{
    protected static function processFile(string $file, bool $globbed, array $arguments): string
    {
        $content = '/' . PathUtility::stripPathSitePrefix($file);

        // Apply versionNumberInFilename setting, but only
        // if the file was not found via glob
        if (!$globbed) {
            switch ($GLOBALS['TYPO3_CONF_VARS']['FE']['versionNumberInFilename'] ?? '') {
                case 'querystring':
                    $content .= '?' . filemtime($file);
                    break;
                case 'embed':
                    $ext = pathinfo($file, PATHINFO_EXTENSION);
                    $content = substr($content, 0, -strlen($ext)) . filemtime($file) . '.' . $ext;
                    break;
            }
        }

        return  $content;
    }
}
