<?php
declare(strict_types=1);
namespace GrossbergerGeorg\BootstrapMinimal\ViewHelpers\Asset;

/*
 * Copyright 2020 by Georg Großberger <contact@grossberger-ge.org>
 *
 * This is free software; it is provided under the terms of Apache License 2.0
 * See the file LICENSE or <https://www.apache.org/licenses/LICENSE-2.0> for details
 */

use TYPO3\CMS\Core\Resource\File;
use TYPO3\CMS\Core\Resource\FileInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Service\ImageService;
use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;
use TYPO3Fluid\Fluid\Core\ViewHelper\Traits\CompileWithContentArgumentAndRenderStatic;

/**
 * Description
 *
 * @author Georg Großberger <g.grossberger@supseven.at>
 */
class InlinePreviewViewHelper extends AbstractViewHelper
{
    use CompileWithContentArgumentAndRenderStatic;

    protected $escapeChildren = false;

    protected $escapeOutput = false;

    public function initializeArguments()
    {
        $this->registerArgument('file', FileInterface::class, 'File to include');
        $this->registerArgument('size', 'int', 'Max width and height of the preview', false, 100);
        $this->registerArgument('fallback', 'string', 'Max width and height of the preview', false, 'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7');
    }

    public static function renderStatic(
        array $arguments,
        \Closure $renderChildrenClosure,
        RenderingContextInterface $renderingContext
    ) {
        $file = $renderChildrenClosure();

        if ($file instanceof FileInterface && (int) $file->getProperty('type') === File::FILETYPE_IMAGE) {
            $imageService = GeneralUtility::makeInstance(ImageService::class);
            $preview = $imageService->applyProcessingInstructions($file, [
                'maxHeight' => $arguments['size'],
                'maxWidth'  => $arguments['size'],
                'quality'   => 30,
            ]);

            $content = sprintf(
                'data:%s;base64,%s',
                $preview->getMimeType(),
                base64_encode($preview->getContents())
            );
        } else {
            $content = $arguments['fallback'];
        }

        return $content;
    }
}
