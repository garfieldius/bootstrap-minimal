<?php
declare(strict_types=1);
namespace GrossbergerGeorg\BootstrapMinimal\Rendering;

/*
 * Copyright 2020 by Georg Großberger <contact@grossberger-ge.org>
 *
 * This is free software; it is provided under the terms of Apache License 2.0
 * See the file LICENSE or <https://www.apache.org/licenses/LICENSE-2.0> for details
 */

/**
 * PHP functions called as postUserFunc in
 *
 * @author Georg Großberger <contact@grossberger-ge.org>
 */
class TypoScriptCallback
{
    /**
     * Convert the CType value of a tt_content record to
     * the template name of the element
     *
     * @param $type
     * @param $config
     * @return string
     */
    public function typeToTemplateName($type, $config): string
    {
        if (!empty($config['map.'][$type])) {
            $name = $config['map.'][$type];
        } else {
            if (strlen($type) > 3 && strncmp($type, 'tx_', 3) === 0) {
                $type = substr($type, 3);
            }

            $name = str_replace(['_', '-'], ' ', $type);
            $name = ucwords($name);
            $name = str_replace(' ', '', $name);
        }

        return $name;
    }

    /**
     * Replace markers in the given content with
     * data from the TSFE
     *
     * @param $content
     * @param $conf
     * @return string
     */
    public function replaceMarkers($content, $conf): string
    {
        if (is_array($conf) && !empty($conf)) {
            foreach ($conf as $type => $marker) {
                if (strpos($content, $marker) !== false) {
                    switch ($type) {
                        case 'title':
                            $title = $GLOBALS['TSFE']->generatePageTitle();
                            $title = htmlspecialchars($title, ENT_COMPAT | ENT_HTML5, 'UTF-8', false);
                            $content = str_replace($marker, $title, $content);
                            break;
                        case 'headerData':
                            $content = str_replace($marker, implode('', $GLOBALS['TSFE']->additionalHeaderData), $content);
                            break;
                        case 'footerData':
                            $content = str_replace($marker, implode('', $GLOBALS['TSFE']->additionalFooterData), $content);
                            break;
                    }
                }
            }
        }

        return $content;
    }
}
