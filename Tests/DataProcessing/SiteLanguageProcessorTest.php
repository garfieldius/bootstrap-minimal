<?php
declare(strict_types=1);
namespace GrossbergerGeorg\BootstrapMinimal\Tests\DataProcessing;

/*
 * Copyright 2020 by Georg Großberger <contact@grossberger-ge.org>
 *
 * This is free software; it is provided under the terms of Apache License 2.0
 * See the file LICENSE or <https://www.apache.org/licenses/LICENSE-2.0> for details
 */

use GrossbergerGeorg\BootstrapMinimal\DataProcessing\SiteLanguageProcessor;
use GuzzleHttp\Psr7\Uri;
use PHPUnit\Framework\TestCase;
use TYPO3\CMS\Core\Http\ServerRequest;
use TYPO3\CMS\Core\Site\Entity\SiteLanguage;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;

/**
 * SiteLanguageProcessorTest
 *
 * @author Georg Großberger <contact@grossberger-ge.org>
 */
class SiteLanguageProcessorTest extends TestCase
{
    protected function setUp(): void
    {
        unset($GLOBALS['TYPO3_REQUEST']);
    }

    /**
     * @dataProvider processData
     * @param ServerRequest|null $request
     * @param SiteLanguage|null $expectedLanguage
     */
    public function testProcess(?ServerRequest $request, ?SiteLanguage $expectedLanguage)
    {
        $cObj = $this->createMock(ContentObjectRenderer::class);
        $expected = [];
        $as = 'language';

        if ($expectedLanguage) {
            $expected[$as] = $expectedLanguage;
        }

        if ($request) {
            $GLOBALS['TYPO3_REQUEST'] = $request;
        }

        $subject = new SiteLanguageProcessor();
        $actual = $subject->process($cObj, [], ['as' => $as], []);

        static::assertEquals($expected, $actual);
    }

    public function processData()
    {
        $datasets = [];

        $datasets[] = [null, null];

        $request1 = $this->createMock(ServerRequest::class);
        $request1->expects(static::any())->method('getAttribute')->with('language')->willReturn(null);

        $datasets[] = [$request1, null];

        $language = new SiteLanguage(0, 'en', new Uri('http://localhost/de'), []);

        $request2 = $this->createMock(ServerRequest::class);
        $request2->expects(static::any())->method('getAttribute')->with('language')->willReturn($language);

        $datasets[] = [$request2, $language];

        return $datasets;
    }
}
