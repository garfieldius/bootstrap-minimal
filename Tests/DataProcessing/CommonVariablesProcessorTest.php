<?php
declare(strict_types=1);
namespace GrossbergerGeorg\BootstrapMinimal\Tests\DataProcessing;

/*
 * Copyright 2020 by Georg Großberger <contact@grossberger-ge.org>
 *
 * This is free software; it is provided under the terms of Apache License 2.0
 * See the file LICENSE or <https://www.apache.org/licenses/LICENSE-2.0> for details
 */

use GrossbergerGeorg\BootstrapMinimal\DataProcessing\CommonVariablesProcessor;
use PHPUnit\Framework\TestCase;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;

/**
 * @author Georg Großberger <contact@grossberger-ge.org>
 */
class CommonVariablesProcessorTest extends TestCase
{
    public function testProcess()
    {
        $data = [
            'uid' => 1,
        ];

        $page = [
            'uid' => 2,
        ];

        $cObj = $this->createMock(ContentObjectRenderer::class);
        $cObj->data = $data;

        $GLOBALS['TSFE'] = new \stdClass();
        $GLOBALS['TSFE']->page = $page;

        $expected = [
            'data' => $data,
            'page' => $page,
        ];

        $subject = new CommonVariablesProcessor();
        $actual = $subject->process($cObj, [], [], []);

        static::assertEquals($expected, $actual);
    }
}
