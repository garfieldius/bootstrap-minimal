<?php
declare(strict_types=1);
namespace GrossbergerGeorg\BootstrapMinimal\Tests\DataProcessing;

/*
 * Copyright 2020 by Georg Großberger <contact@grossberger-ge.org>
 *
 * This is free software; it is provided under the terms of Apache License 2.0
 * See the file LICENSE or <https://www.apache.org/licenses/LICENSE-2.0> for details
 */

use GrossbergerGeorg\BootstrapMinimal\DataProcessing\GalleryVariantsProcessor;
use GrossbergerGeorg\BootstrapMinimal\Gallery\GalleryBuilder;
use PHPUnit\Framework\TestCase;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;

/**
 * GalleryVariantsProcessorTest
 *
 * @author Georg Großberger <contact@grossberger-ge.org>
 */
class GalleryVariantsProcessorTest extends TestCase
{
    public function testProcess()
    {
        $record = [
            'uid'         => 1,
            'imagewidth'  => 800,
            'imageheight' => 600,
        ];

        $cObj = $this->createMock(ContentObjectRenderer::class);
        $cObj->data = $record;

        $rendererOptions = [
            'showinfo' => true,
        ];

        $expectedOptions = [
            'no-cookie'      => true,
            'modestbranding' => true,
            'relatedVideos'  => false,
            'showinfo'       => true,
        ];

        $expected = [
            'gallery' => [
                'pic1'
            ],
        ];

        $expectedDefaultSize = $record['imagewidth'] . 'x' . $record['imageheight'];

        $builder = $this->createMock(GalleryBuilder::class);
        $builder->expects(static::once())
            ->method('createGallery')
            ->with(static::equalTo($record), static::equalTo($expectedOptions), static::equalTo($expectedDefaultSize))
            ->willReturn($expected['gallery']);

        GeneralUtility::addInstance(GalleryBuilder::class, $builder);

        $subject = new GalleryVariantsProcessor();
        $actual = $subject->process($cObj, [], ['rendererOptions' => $rendererOptions], [], []);

        static::assertEquals($expected, $actual);
    }
}
