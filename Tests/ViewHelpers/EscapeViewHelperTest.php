<?php
declare(strict_types=1);
namespace GrossbergerGeorg\BootstrapMinimal\Tests\ViewHelpers;

/*
 * Copyright 2020 by Georg Großberger <contact@grossberger-ge.org>
 *
 * This is free software; it is provided under the terms of Apache License 2.0
 * See the file LICENSE or <https://www.apache.org/licenses/LICENSE-2.0> for details
 */

use GrossbergerGeorg\BootstrapMinimal\ViewHelpers\EscapeViewHelper;
use PHPUnit\Framework\TestCase;
use TYPO3Fluid\Fluid\Core\Rendering\RenderingContext;

/**
 * EscapeViewHelperTest
 *
 * @author Georg Großberger <contact@grossberger-ge.org>
 */
class EscapeViewHelperTest extends TestCase
{
    /**
     * @covers \GrossbergerGeorg\BootstrapMinimal\ViewHelpers\EscapeViewHelper
     * @dataProvider escapingDataProvider
     * @param string $source
     * @param string $expected
     */
    public function testEscaping(string $source, string $expected)
    {
        $context = $this->createMock(RenderingContext::class);
        $renderingClosure = function () use ($source) {
            return $source;
        };

        $actual = EscapeViewHelper::renderStatic([], $renderingClosure, $context);

        self::assertEquals($expected, $actual);
    }

    public function escapingDataProvider()
    {
        return [
            ['href="index.php?id=1&type=0"', 'href=&quot;index.php?id=1&amp;type=0&quot;'],
            ['href="index.php?id=1&amp;type=0"', 'href=&quot;index.php?id=1&amp;type=0&quot;'],
            ['href="index.php?id=1&type=0&quot;', 'href=&quot;index.php?id=1&amp;type=0&quot;'],
        ];
    }
}
