<?php
declare(strict_types=1);

/*
 * Copyright 2020 by Georg Großberger <contact@grossberger-ge.org>
 *
 * This is free software; it is provided under the terms of Apache License 2.0
 * See the file LICENSE or <https://www.apache.org/licenses/LICENSE-2.0> for details
 */

$EM_CONF[$_EXTKEY] = [
    'title'            => 'Bootstrap Minimal',
    'description'      => 'Minimal yet fully integrated theme for TYPO3 using bootstrap HTML only',
    'version'          => '1.0.0',
    'state'            => 'stable',
    'category'         => 'misc',
    'clearCacheOnLoad' => 0,
    'constraints'      => [
        'depends'   => [],
        'conflicts' => [
            'fluid_styled_content' => '9.5.0-10.4.999'
        ],
        'suggests' => [],
    ],
];
