<?php
declare(strict_types=1);

/*
 * Copyright 2020 by Georg Großberger <contact@grossberger-ge.org>
 *
 * This is free software; it is provided under the terms of Apache License 2.0
 * See the file LICENSE or <https://www.apache.org/licenses/LICENSE-2.0> for details
 */

$GLOBALS['TCA']['tt_content']['columns']['space_before_class']['config']['items'][1][1] = 'mt-1';
$GLOBALS['TCA']['tt_content']['columns']['space_before_class']['config']['items'][2][1] = 'mt-2';
$GLOBALS['TCA']['tt_content']['columns']['space_before_class']['config']['items'][3][1] = 'mt-3';
$GLOBALS['TCA']['tt_content']['columns']['space_before_class']['config']['items'][4][1] = 'mt-4';
$GLOBALS['TCA']['tt_content']['columns']['space_before_class']['config']['items'][5][1] = 'mt-5';

$GLOBALS['TCA']['tt_content']['columns']['space_after_class']['config']['items'][1][1] = 'mb-1';
$GLOBALS['TCA']['tt_content']['columns']['space_after_class']['config']['items'][2][1] = 'mb-2';
$GLOBALS['TCA']['tt_content']['columns']['space_after_class']['config']['items'][3][1] = 'mb-3';
$GLOBALS['TCA']['tt_content']['columns']['space_after_class']['config']['items'][4][1] = 'mb-4';
$GLOBALS['TCA']['tt_content']['columns']['space_after_class']['config']['items'][5][1] = 'mb-5';

// Remove bullets CE
unset($GLOBALS['TCA']['tt_content']['types']['bullets']);

foreach ($GLOBALS['TCA']['tt_content']['columns']['CType']['config']['items'] as $i => $item) {
    if ($item[1] == 'bullets') {
        unset($GLOBALS['TCA']['tt_content']['columns']['CType']['config']['items'][$i]);
        break;
    }
}

unset($i, $item);
