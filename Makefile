
.PHONY: build
build: vendor/autoload.php

.PHONY: compile
compile: Resources/Public/JavaScript/init.js

.PHONY: clean
clean:
	@rm -f vendor/autoload.php .php_cs.cache .phpunit.result.cache

.PHONY: clobber
clobber: clean
	@rm -rf vendor bin

.PHONY: fix
fix: bin/php-cs-fixer vendor/autoload.php
	@./bin/php-cs-fixer fix --config=.php_cs -vvv

.PHONY: test
test: bin/phpunit vendor/autoload.php
	@./bin/phpunit -c phpunit.xml

vendor/autoload.php bin/php-cs-fixer bin/phpunit: bin/composer.phar composer.json composer.lock
	@php bin/composer.phar install --no-plugins --no-scripts -n --no-suggest --ansi \
	&& touch vendor/autoload.php bin/php-cs-fixer bin/phpunit

bin/composer.phar:
	@php -r '@mkdir("bin");' \
	&& php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" \
	&& php -r "if (hash_file('sha384', 'composer-setup.php') === 'e0012edf3e80b6978849f5eff0d4b4e4c79ff1609dd1e613307e16318854d24ae64f26d17af3ef0bf7cfb710ca74755a') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;" \
	&& php composer-setup.php --install-dir=bin/ \
	&& php -r "unlink('composer-setup.php');"

Resources/Public/JavaScript/init.js: Build/node_modules/.yarn-integrity Build/Javascript/init.js
	@mkdir -p Resources/Public/JavaScript \
	&& yarn google-closure-compiler \
		-O ADVANCED \
		-W QUIET \
		--assume_function_wrapper \
		--js_output_file=$(realpath Resources/Public/JavaScript/init.js) \
		$(realpath Build/Javascript/init.js) \
	&& touch Resources/Public/JavaScript/init.js

Build/node_modules/.yarn-integrity: Build/yarn.lock
	@yarn --cwd Build/ install --prefer-offline --frozen-lockfile --silent \
	&& touch Build/node_modules/.yarn-integrity
