<?php

/*
 * Copyright 2020 by Georg Großberger <contact@grossberger-ge.org>
 *
 * This is free software; it is provided under the terms of Apache License 2.0
 * See the file LICENSE or <https://www.apache.org/licenses/LICENSE-2.0> for details
 */

$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\TYPO3\CMS\Backend\Form\Element\ImageManipulationElement::class]['className'] =
    \GrossbergerGeorg\BootstrapMinimal\CropVariants\ExtendedVariantsAwareManipulationElement::class;

if (empty($GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations']['bootstrap_minimal'])) {
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations']['bootstrap_minimal'] = [];
}

if (empty($GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations']['bootstrap_minimal']['frontend'])) {
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations']['bootstrap_minimal']['frontend'] =
        \TYPO3\CMS\Core\Cache\Frontend\VariableFrontend::class;
}
