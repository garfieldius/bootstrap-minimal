
/*
 * (c) 2020 by Georg Großberger <contact@grossberger-ge.org>
 *
 * This is free software; you can redistribute it and/or
 * modify it under the terms of the Apache-2.0 license
 *
 * For the full copyright and license information see
 * <https://www.apache.org/licenses/LICENSE-2.0>
 */

(function (Window, Document, eventName) {
    function onloadCallback() {
        Window.removeEventListener(eventName, onloadCallback);

        [
            [
                'link',
                function (tag, dataAttributeValue) {
                    tag.rel = 'stylesheet';
                    tag.href = dataAttributeValue;
                }
            ],
            [
                'script',
                function (tag, dataAttributeValue) {
                    tag.async = true;
                    tag.defer = true;
                    tag.type = 'text/javascript';
                    tag.src = dataAttributeValue;
                }
            ]
        ].forEach(function (block, cb, dataAttr) {
            dataAttr = '[data-' + block[0] + ']';

            [].forEach.call(
                Document.querySelectorAll(dataAttr),
                function (el, tag) {
                    tag = Document.createElement(block[0]);
                    block[1](tag, el.getAttribute(dataAttr));
                    el.parentNode.insertBefore(tag, el);
                }
            );
        });
    }

    Window.addEventListener(eventName, onloadCallback);
})(window, document, 'load');
